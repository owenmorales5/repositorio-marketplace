import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { API } from '../configuraciones';



@Injectable({
  providedIn: 'root'
})
export class ProductosService {

  private api: string = API.url;
  constructor(private http: HttpClient) { }

  getDatos(){
    return this.http.get(`${this.api}productos.json`);
  }
}
