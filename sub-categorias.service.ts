import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { API } from '../configuraciones';


@Injectable({
  providedIn: 'root'
})
export class SubCategoriasService {

  private api: string = API.url;
  constructor(private http: HttpClient) { }

  getDatosFiltrados(ordenarPor, valor){
    return this.http.get(`${this.api}sub-categorias.json?orderBy="${ordenarPor}"&equalTo="${valor}"&print=pretty`);
  }
}
